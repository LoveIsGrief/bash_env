{
  pkgs ?  (import <nixpkgs> {}),
  version ? (import ./readCommit.nix {}),
  ...
}:

let
  my-python = pkgs.python3;
  pythonAndPackages = my-python.withPackages (p: with p; [
    requests
  ]);
in

pkgs.stdenv.mkDerivation {
  inherit version;
  pname = "loveisgrief-bash";
  buildInputs = with pkgs; [
    bash
    coreutils
    findutils
    gcc
    gnugrep
    gnused
    pythonAndPackages
  ];

  installPhase = ''bash ./nix_builder.sh'';
  src = builtins.path { path = ./. ; name = "loveisgrief-bash"; };
}
