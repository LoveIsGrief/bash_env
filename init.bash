#@IgnoreInspection BashAddShebang

# Make sure only to init once
if [[ -z "$__MY_BASH_INIT_FLAG" ]] ; then
__MY_BASH_INIT_FLAG="initializing"

# We should move to the script dir
# Since this isn't possible using $0 we need our own var :/
pushd $MY_BASH_INIT_DIR > /dev/null

# Set the title of the containing terminal emulator
# Chapter `Window Titles` in `man xterm`
printf '\033]2;'$(hostname)'\007'

#
# Sources all files ending in .bash in the given subfolder
#
sourceFolder(){
    local folder=$1
    # import functions
    if [ -d "$folder" ] ; then
        pushd $folder > /dev/null
        for file in `find -maxdepth 1 -type f -name "*.bash"`; do
            source "$file"
        done 
        popd > /dev/null
    fi
}

#
# import functions
#
sourceFolder "aliases/"
sourceFolder "exports/"
sourceFolder "functions/"
sourceFolder "completions/"


#
# add custom commands
#
__USER_BIN="$HOME/bin"
if ! [ -d "$__USER_BIN" ] ; then
    mkdir "$__USER_BIN"
fi

# set PATH so it includes user's private bin if it exists
#  AND isn't set yet
if [ -d "$__USER_BIN" ] && ! (echo "$PATH" | tr : '\n' | grep -E "^${__USER_BIN}$" > /dev/null ) ; then
    PATH="$__USER_BIN:$PATH"
fi
PATH="$PATH:$MY_BASH_INIT_DIR/bin"


# Installed programs using the ~/.local prefix
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

export HISTCONTROL="ignoredups:ignorespace"

#
# Set shopts
# https://www.gnu.org/software/bash/manual/bashref.html#The-Shopt-Builtin
#

# If set, a command name that is the name of a directory is executed as if it were the argument to the cd command. This option is only used by interactive shells
shopt -s autocd

# Fix the damn globs
# If set, the pattern ‘**’ used in a filename expansion context will match all files and zero or more directories and subdirectories. If the pattern is followed by a ‘/’, only directories and subdirectories match
shopt -s globstar

# Only for interactive shells
if [[ -n "$PS1" ]] ; then
    # Custom key-bindings
    bind -f inputrc

    # Ctr+C deletes the line
    # https://unix.stackexchange.com/questions/376153/how-can-one-override-bash-ctrlc-to-be-more-fish-like
    trap 'tput dl1; tput cuu1; tput dl1; tput cuu1' SIGINT

    #
    # set the commandline format and color
    #
    if [ -e "PS.bash" ] ; then
	    source PS.bash
    fi

    #
    # Extra stuff if it exists
    # Useful for nix package to add generated env vars, etc
    #
    if [ -e "extra_interactive.bash" ] ; then
	source "extra_interactive.bash"
    fi
else
    #
    # Extra stuff if it exists
    # Useful for nix package to add generated env vars, etc
    #
    if [ -e "extra_noninteractive.bash" ] ; then
	source "extra_noninteractive.bash"
    fi
fi

#
# Extra stuff if it exists
# Useful for nix package to add generated env vars, etc
#
if [ -e "extra.bash" ] ; then
    source "extra.bash"
fi

popd > /dev/null

# Indicator that we're done initializing
__MY_BASH_INIT_FLAG="initialized"
fi
