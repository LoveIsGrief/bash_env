# Generates a random string of printable characters
# Default length is 20, otherwise it takes it as the first param

function makepass ()
{ 
    length=$1
    if [ $# -lt 1 ] ; then
            length=20
    fi
    cat /dev/urandom | tr -cd '[:print:]' | head -c $length
    echo
}
