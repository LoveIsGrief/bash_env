dupesInDirs(){
    if [[ $# < 2 ]] ; then
	echo "Please provide at least 2 paths arguments" >&2
	echo "dupesInDirs <path> <path> [<path>...]" >&2
	return 1
    fi
    find "$@" -not -type d -printf '%P\n' | sort | uniq -d
}
