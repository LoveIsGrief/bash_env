# grep ps
# greps the output of `ps faux` and removes grep from the output

gps(){
    ps faux | grep "$1" | grep -v grep
}
