# Reduces the size of a given image
# requires imagemagick to be installed
#   it should have the convert command included
klein(){
    img="${1:-`pwd`}"
    out="${2:-${img}_lesser.jpg}"

    convert "$img" -resize 60% -quality 70 "$out"
}
