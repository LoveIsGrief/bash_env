. __log.bash

cdl () 
{ 
    __log "args: $@";
    if [[ $# -gt 0 ]] ; then
	path="$1"
        if [[ -f "$1" ]] ; then
            path=`dirname "$1"`
        fi
    else
	path="$HOME"
    fi
    if cd "$path" ; then
        ls --color=auto -alFh
    fi
}
