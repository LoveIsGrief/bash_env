# Print the mimetype of a file
# `file` actually guesses it from the file content
#  not the filename!

mime(){
    file -i "$1"
}
