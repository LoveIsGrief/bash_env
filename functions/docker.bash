#######
# Shortcuts for docker
#######

# Docker exec
dex(){
    docker exec -it $1 env TERM=xterm bash -l
}

_container_name(){
    echo "`echo "$1" | tr /: _`_`date +%s`"
}

# Docker run & exec
drun(){
    local image=$1
    local container_name=${2:-"`_container_name $1`"}
    docker run --privileged -dti --name $container_name $image /bin/bash
    dex $container_name
}

# Docker run, exec and kill
drunk(){
    local container_name="`_container_name $1`"
    drun $1 $container_name
    docker rm -f $container_name
}

# Docker ps all
dps(){
    docker ps -a
}

# Docker remove all none images
drami(){
    docker rmi `docker images --format "{{.ID}} {{.Repository}}" | grep none | cut -d ' ' -f 1`
}
