# Activates a git environment to version nixos configuration files
function _gitnix(){
    if [[ -n "${_GIT_NIX_SET:-}" ]] ; then
        export -n GIT_WORK_TREE
        export -n GIT_DIR
        unset GIT_WORK_TREE
        unset GIT_DIR
        unset _GIT_NIX_SET
        echo "deactivated git env vars for nix config versioning"
    else
        # TODO make these configurable
        DEFAULT_NIXOS_DIR=/etc/nixos
        DEFAULT_GIT_DIR="${XDG_CONFIG_HOME:-$HOME/.config}/nixos/config/.git"

        export GIT_WORK_TREE="${MY_NIXOS_DIR:-$DEFAULT_NIXOS_DIR}"
        export GIT_DIR="${MY_NIXOS_GIT_DIR:-$DEFAULT_GIT_DIR}"

        if ! [[ -d "${GIT_DIR}" ]] ; then
            mkdir -p "${GIT_DIR}"
            git init
        fi

        _GIT_NIX_SET=1
        echo "activated git env vars for nix config versioning"
    fi
}
