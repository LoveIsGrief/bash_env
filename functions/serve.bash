serve(){
    dir=${1:-`pwd`}
    port=${2:-8080}
    host=${3:-localhost}
    tempconf=/tmp/lighttpd_${port}.conf

    # Loop until we have a port unused by another lighttpd instance
    while ls $tempconf &> /dev/null ; do
	port=$(expr $port + 1 )
	tempconf=/tmp/lighttpd_${port}.conf
    done

    # Create the temporary config
    echo "server.document-root = \"${dir}\" 
    server.port          = $port
    server.bind          = \"$host\"
    dir-listing.activate = \"enable\"" > $tempconf
    echo "serving '$dir' on http://$host:$port"

    lighttpd -Df $tempconf

    # Cleanup, though a trap might be better
    rm $tempconf
}
