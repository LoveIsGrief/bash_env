ssh-add-all(){
    # Open kwallet first to stop multiple password requests
    kwallet-query -l kdewallet &> /dev/null
    # TODO rewrite because of < /dev/null applying to `find` instead of ssh-add
    find $HOME/.ssh/keys -type f ! -name "*.pub" -exec ssh-add "{}" < /dev/null \;
}

