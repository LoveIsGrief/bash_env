# A function to have a list of commands that could help when doing shit...
# It uses vim because of it's possibility to link in help files
# And one doesn't need a converter for markdown or whatever
# type ":help help-writing" for more information and a nice little rabbit hole

if which vim &> /dev/null ; then
    function helpful(){
    (
        cd "${_helpfulCommandsDir}"
        # Generate tags at each call
        vim -R -c "helpt ++t ." index.txt
    )
    }
fi
