{...}:

with builtins;
let
  headFile = readFile ./.git/HEAD;
  headStr = replaceStrings ["ref: " "\n"] ["" ""] headFile;
  headPath = ./.git + ( "/" + headStr );
  commitFile = readFile headPath;
  commitHash = replaceStrings ["\n"] [""] commitFile;
in
  builtins.hashFile "sha256" headPath

