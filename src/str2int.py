#!/usr/bin/env python3

def str2int(string):

    out = 0
    for i in string:
        out += ord(i)
    return out

if __name__ == "__main__":
    import sys
    print(str2int(sys.argv[1]))
