set -e
source $stdenv/setup

# Prepare a temporary directory to work in
# this will also work when called from nix-shell
distDir="$(pwd)/dist"
rm -rf "$distDir"
cp -r "$src" "$distDir"
chmod +w -R "$distDir"
cd "$distDir"

# filter out unnecessary stuff since builtins.filterSource doesn't work
rm -rf result .idea .git *.nix nix_* dist/ *.swp

# Finish off by putting the files in the output directory for nix to find
cp -r $distDir $out
