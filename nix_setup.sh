unset PATH
for p in $buildInputs; do
  export PATH=$p/bin${PATH:+:}$PATH
done

PYTHONPATH=${pythonPath}
