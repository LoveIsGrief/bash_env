# bash_env
My bash environment

# Optional requirements

 - helix
 - vim
 - tmuxinator

# Usage

    git clone https://gitlab.com/LoveIsGrief/bash_env.git /a/path/of/your/choosing

Add these lines to your `.profile`, `.bashrc` or other file  that is read by bash at startup

```sh
MY_BASH_INIT_DIR=/a/path/of/your/choosing/
. ${MY_BASH_INIT_DIR}/init.bash
```

## nixOS

You can install the package manually `/etc/nixos/loveisgriefBash.nix` with the following contents
that you adapt by changing the revision.

```nix
{ config, pkgs, ... }:
let
  rev = 'PASS THE COMMITISH HERE';
  loveisgriefBash = import (pkgs.fetchFromGitLab {
      owner = "LoveIsGrief";
      repo = "bash_env";
      inherit rev;
      sha256 = "";
  }) {
    inherit pkgs;
    version = rev;
  };
in
{

  environment.systemPackages = with pkgs;[
    tmux
    loveisgriefBash
  ];
}
```

And add

```sh
MY_BASH_INIT_DIR="$(bash_env)"
[[ -n "${MY_BASH_INIT_DIR}" ]] && . "${MY_BASH_INIT_DIR}/init.bash"
```
to your `~/.profile`.

