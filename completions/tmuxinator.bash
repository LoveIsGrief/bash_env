# Only try to add bash completion if it exists
if which tmuxinator &> /dev/null ; then
    tmuxinator_completion_file="$(dirname `gem which tmuxinator`)/../completion/tmuxinator.bash"
    if [[ -e "${tmuxinator_completion_file}" ]] ; then
        source "${tmuxinator_completion_file}"
    fi
fi

