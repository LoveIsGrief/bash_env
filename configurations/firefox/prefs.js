// Automatically enable extensions found in the profile
// http://kb.mozillazine.org/About:config_entries
user_pref("extensions.autoDisableScopes", 0);
user_pref("datareporting.policy.firstRunURL", "");
user_pref("browser.tabs.warnOnClose", false);
user_pref("browser.tabs.warnOnCloseOtherTabs", false);

