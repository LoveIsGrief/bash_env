# Set the default editor by order of priority
for editor in hx vim vi
do
  if which "$editor" &> /dev/null ; then
    export VISUAL="$editor"
    export EDITOR="$editor"
    export GIT_EDITOR="$editor"
    break
  fi
done

