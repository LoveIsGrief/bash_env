# Custom bash prompt via http://kirsle.net/wizards/ps1.html
# TERM isn't automatically set when logging in
# we shouldn't shit the bed when using tput
if [[ ${TERM:-dumb} != dumb ]] ; then

    # TODO change the color of the name too

    # Generate a color between 2 and 7
    # 0 = black and 1 = red
    # red is reserved for remote hosts
    getHostNameColor(){
	local hostNum=$("$MY_BASH_INIT_DIR/src/str2int.py" `hostname`)
	expr $hostNum % 6 + 2
    }

    # Allows setting the host color before hand
    if [[ -z "$PS1_HOST_COLOR" ]] ; then
	# For ssh connections, we must know we're on a remote host
	# Thefore the host is red
	if [[ -n "$SSH_CONNECTION" ]] ; then
	    PS1_HOST_COLOR=1
	else
	    # As a last resort the color will be chosen randomly
	    PS1_HOST_COLOR=`getHostNameColor`
	fi
    fi

    export PS1="\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf $PS1_HOST_COLOR)\]\h \[$(tput setaf 5)\]\w\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\n\\$ \[$(tput sgr0)\]"
fi
