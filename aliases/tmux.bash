# tmux with this conf
alias tmux="command tmux -f \"${MY_BASH_INIT_DIR}/configurations/tmux/basic.conf\""

# Tiled tmux / 4pane tmux
alias ttmux="tmux new \; source-file \"${MY_BASH_INIT_DIR}/configurations/tmux/4pane-session.conf\""