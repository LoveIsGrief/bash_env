#!/usr/bin/env bash
# Creates ssh aliases of each host in your /etc/hosts file

OLD_IFS="${IFS}"
IFS=$(echo -e "\n\b")
# All valid host lines
for line in `egrep -v '^#' /etc/hosts | egrep -v '^\s*$'`
do
    # make an array
    IFS=$(echo -en " \b \t") read -a tuple <<< "${line}"
    # TODO handle multiple hosts per IP
    host=${tuple[1]}
    alias ${host}="ssh $host"
done
IFS="${OLD_IFS}"

