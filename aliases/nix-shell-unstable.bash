# Start a nix-shell using unstable nixpkgs
alias nix-shell-unstable="nix-shell -I nixpkgs=https://github.com/NixOS/nixpkgs/archive/refs/heads/nixpkgs-unstable.zip -p"

