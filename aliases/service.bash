# A wrapper around service in order to allow taking actions on multiple services
# e.g
#   service celeryd* status
#   service redis* restart

service(){
    if [[ -n "$1" && -n "$2" ]] ; then
        local _command="$2"
        local _services=(`find /etc/init.d/ -name "$1" -printf '%f\n'`)
        shift 2
        echo -e "Executing ${_command} on ${_services[*]}\n"
        for s in "${_services[@]}" ; do
            sudo service $s $_command $*
        done
    else
        sudo service $*
    fi
}
