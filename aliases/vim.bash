# Install .vimrc requirements

if which vim &> /dev/null ; then
	## Plugins using vimplug
	## https://github.com/junegunn/vim-plug
	if ! [[ -f ~/.vim/autoload/plug.vim ]]
	then
		curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
		https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	fi

	# vim with custom conf
	alias vim="command vim -u \"${MY_BASH_INIT_DIR}/configurations/vim/vimrc\""
fi

