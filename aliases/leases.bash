# Get DHCP leases
# Useful to find out the IPs when sharing
#  WiFi over Ethernet
alias leases='cat /var/lib/misc/dnsmasq.leases'
